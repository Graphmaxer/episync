use std::error::Error;
use std::ffi::OsString;
use std::fs::File;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct EpiProject {
    pub owner: String,
    pub year: String,
    pub module: String,
    pub repository: String,
}

pub fn parse_projects_file(file_path: OsString) -> Result<Vec<EpiProject>, Box<dyn Error>> {
    let file = File::open(file_path)?;
    let mut reader = csv::Reader::from_reader(file);
    let mut projects: Vec<EpiProject> = vec![];
    for line in reader.deserialize() {
        projects.push(line?);
    }
    Ok(projects)
}
