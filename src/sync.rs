use std::error::Error;

use crate::config::Config;
use crate::git::{add_remote, clone_project, pust_to_remote};
use crate::gitlab::create_project_repository;
use crate::parse::EpiProject;

pub fn sync_project(config: &Config, epiproject: &EpiProject) -> Result<(), Box<dyn Error>> {
    create_project_repository(config, epiproject)?;
    clone_project(epiproject)?;
    add_remote(config, epiproject)?;
    pust_to_remote(epiproject)?;
    Ok(())
}
