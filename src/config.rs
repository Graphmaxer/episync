use gitlab::Gitlab;
use serde::Deserialize;

use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

#[derive(Debug, Deserialize)]
pub struct ConfigFile {
    pub root_project: String,
    pub gitlab_private_key: String,
}

pub struct Config {
    pub config_file: ConfigFile,
    pub gitlab_client: Gitlab,
}

pub fn parse_config_file() -> Result<ConfigFile, Box<dyn Error>> {
    let file = File::open(Path::new("./config.json"))?;
    let reader = BufReader::new(file);
    let config_file: ConfigFile = serde_json::from_reader(reader)?;
    Ok(config_file)
}

pub fn create_config(config_file: ConfigFile, gitlab_client: Gitlab) -> Config {
    Config {
        config_file,
        gitlab_client,
    }
}
