mod config;
mod git;
mod gitlab;
mod parse;
mod sync;

use std::env;
use std::error::Error;
use std::ffi::OsString;
use std::process;

use config::create_config;
use log::LevelFilter;
use simplelog::{ColorChoice, TermLogger, TerminalMode};
use sync::sync_project;

use crate::config::parse_config_file;

use crate::gitlab::create_client;
use crate::parse::parse_projects_file;

#[macro_use]
extern crate log;

fn get_file_path() -> Result<OsString, Box<dyn Error>> {
    match env::args_os().nth(1) {
        None => Err(From::from("expected file argument, but got none")),
        Some(file_path) => Ok(file_path),
    }
}

fn run() -> Result<(), Box<dyn Error>> {
    let config_file = parse_config_file()?;
    let client = create_client(&config_file.gitlab_private_key)?;
    let config = create_config(config_file, client);
    let projects = parse_projects_file(get_file_path()?)?;
    for project in projects.iter() {
        sync_project(&config, project)?;
    }
    Ok(())
}

fn main() {
    TermLogger::init(
        LevelFilter::Info,
        simplelog::Config::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )
    .unwrap();
    if let Err(err) = run() {
        error!("{}", err);
        process::exit(1);
    }
}
