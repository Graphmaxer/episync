use std::error::Error;
use std::path::Path;
use std::process::Command;

use crate::config::Config;
use crate::parse::EpiProject;

pub fn clone_project(epiproject: &EpiProject) -> Result<(), Box<dyn Error>> {
    let url = format!(
        "git@git.epitech.eu:{}/{}",
        epiproject.owner, epiproject.repository
    );
    if Path::new(&format!("./sync/{}", epiproject.repository)).exists() {
        Command::new("git")
            .args(&[
                "-C".to_string(),
                format!("./sync/{}", epiproject.repository),
                "pull".to_string(),
            ])
            .output()?;
        info!("{} pulled!", epiproject.repository);
    } else {
        info!("Cloning {}...", url);
        Command::new("git")
            .args(&[
                "clone".to_string(),
                url,
                format!("./sync/{}", epiproject.repository),
            ])
            .output()?;
        info!("Cloning done!");
    }
    Ok(())
}

pub fn add_remote(config: &Config, epiproject: &EpiProject) -> Result<(), Box<dyn Error>> {
    let remote = format!(
        "git@gitlab.com:{}/{}/{}/{}.git",
        config.config_file.root_project, epiproject.year, epiproject.module, epiproject.repository
    );
    Command::new("git")
        .args(&[
            "-C".to_string(),
            format!("./sync/{}", epiproject.repository),
            "remote".to_string(),
            "add".to_string(),
            "gitlab".to_string(),
            remote.clone(),
        ])
        .output()?;
    info!("{} remote added!", remote);
    Ok(())
}

pub fn pust_to_remote(epiproject: &EpiProject) -> Result<(), Box<dyn Error>> {
    Command::new("git")
        .args(&[
            "-C".to_string(),
            format!("./sync/{}", epiproject.repository),
            "push".to_string(),
            "gitlab".to_string(),
            "master".to_string(),
        ])
        .output()?;
    info!("{} pushed!", epiproject.repository);
    Ok(())
}
