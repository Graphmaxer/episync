use std::error::Error;

use gitlab::{
    api::{self, Query},
    Gitlab, GitlabError, Group, Project,
};

use crate::{config::Config, parse::EpiProject};

pub fn create_client(private_key: &str) -> Result<Gitlab, GitlabError> {
    Gitlab::new("gitlab.com", private_key)
}

fn get_group(client: &Gitlab, name: String) -> Result<Group, Box<dyn Error>> {
    let endpoint = api::groups::Group::builder().group(name).build()?;
    let group: Group = endpoint.query(client)?;
    Ok(group)
}

fn find_root_group(config: &Config) -> Result<Group, Box<dyn Error>> {
    get_group(
        &config.gitlab_client,
        config.config_file.root_project.clone(),
    )
}

fn create_group(
    client: &Gitlab,
    name: &str,
    full_path: &str,
    parent_id: u64,
) -> Result<Group, Box<dyn Error>> {
    if let Ok(group) = get_group(client, full_path.to_string()) {
        info!("{} group already exists", full_path);
        return Ok(group);
    }
    let endpoint = api::groups::CreateGroup::builder()
        .name(name)
        .path(name)
        .parent_id(parent_id)
        .build()?;
    let group: Group = endpoint.query(client)?;
    info!("{} group created", full_path);
    Ok(group)
}

fn get_project(client: &Gitlab, name: String) -> Result<Project, Box<dyn Error>> {
    let endpoint = api::projects::Projects::builder()
        .owned(true)
        .search(name.clone())
        .build()?;
    let projects: Vec<Project> = endpoint.query(client)?;
    for project in projects.iter() {
        if project.name == name {
            return Ok(project.clone());
        }
    }
    Err(Box::new(GitlabError::NoResponse {}))
}

fn create_project(
    client: &Gitlab,
    name: &str,
    namespace_id: u64,
) -> Result<Project, Box<dyn Error>> {
    if let Ok(project) = get_project(client, name.to_string()) {
        info!("{} already exists", name);
        return Ok(project);
    }
    let endpoint = api::projects::CreateProject::builder()
        .name(name)
        .namespace_id(namespace_id)
        .lfs_enabled(false)
        .jobs_enabled(false)
        .wiki_enabled(false)
        .issues_enabled(false)
        .packages_enabled(false)
        .snippets_enabled(false)
        .merge_requests_enabled(false)
        .container_registry_enabled(false)
        .pages_access_level(api::projects::FeatureAccessLevelPublic::Disabled)
        .operations_access_level(api::projects::FeatureAccessLevel::Disabled)
        .analytics_access_level(api::projects::FeatureAccessLevel::Disabled)
        .forking_access_level(api::projects::FeatureAccessLevel::Disabled)
        .build()?;
    let project: Project = endpoint.query(client)?;
    info!("{} project created", name);
    Ok(project)
}

pub fn create_project_repository(
    config: &Config,
    epiproject: &EpiProject,
) -> Result<(), Box<dyn Error>> {
    let root = find_root_group(config)?;
    let year_group = create_group(
        &config.gitlab_client,
        &epiproject.year,
        &format!("{}/{}", config.config_file.root_project, epiproject.year),
        root.id.value(),
    )?;
    let module_group = create_group(
        &config.gitlab_client,
        &epiproject.module,
        &format!(
            "{}/{}/{}",
            config.config_file.root_project, epiproject.year, epiproject.module
        ),
        year_group.id.value(),
    )?;
    create_project(
        &config.gitlab_client,
        &epiproject.repository,
        module_group.id.value(),
    )?;
    Ok(())
}
